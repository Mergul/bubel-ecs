/************************************************************************************************************************
This module contain main helper templates for user.
There are three structure templates (mixins) which can be added on top of structure:
$(LIST
	* System: make system structure
	* Component: make component structure
	* Event: make event structure
)
This mixins are optional and are used to adding some additional capabilities, e.g. ECS.System is used to configuring default number of jobs for system.

---
Struct System1
{
	mixin!ECS.System;
}

Struct System2
{
	mixin!ECS.System(16);//set number of jobs generated for system by multithreaded update
}

Struct Component1
{
	mixin!ECS.Component;
}

Struct Event1
{
	mixin!ECS.Event;
}
---

There is also template for generating list of excluded components "ExcludedComponets(T...)".
This template takes component structure types and making list of excluded components used in "registerSystem" function.

---
Struct System1
{
	mixin!ECS.System;

	struct EntitiesData
	{
		... //used components
	}

	ExcludedComponets!(Comp1, Comp2);
}
---

Templates ReadOnlyDependencies nad WritableDependencies are used to create list of dependencies for System.
Writable dependencies are bloking parallel execution of system which has same dependency (as writable or readonly).
This dependencies works same as Component dependencies but can be used for creating external dependencies (e.g. dependency on spatial partitioning tree access).

---
enum ExternalDependency1 = "ExternalDependency1";

Struct System1
{
	mixin!ECS.System;

	struct EntitiesData
	{
		... //used components
	}

	ReadOnlyDependencies!(ExternalDependency1);
}
---

Copyright: Copyright © 2018-2023, Dawid Masiukiewicz, Michał Masiukiewicz
License: BSD 3-clause, see LICENSE file in project root folder.
*/
module bubel.ecs.core;

public import bubel.ecs.manager;
public import bubel.ecs.entity;
public import bubel.ecs.traits : becsID;

/************************************************************************************************************************
Main struct used as namespace for templates.
*/
static struct ECS
{
	/************************************************************************************************************************
	Set default system parameters (number of parallel jobs)
	*/
	mixin template System(uint jobs_count = 32)
	{
		__gshared uint __becs_jobs_count = jobs_count;
	}

	/************************************************************************************************************************
	Mark structure as Component
	*/
	mixin template Component()
	{
		deprecated ComponentRef ref_() @nogc nothrow return 
		{
			return ComponentRef(&this, becsID!(typeof(this)));
		}
	}

	/************************************************************************************************************************
	Mark structure as Event
	*/
	mixin template Event()
	{
		
	}

	/************************************************************************************************************************
	Make list of excluded components. This template get structure types as argument. Should be added inside System structure.
	*/
	mixin template ExcludedComponents(T...)
	{
		alias ExcludedComponents = T;
	}

	/************************************************************************************************************************
	Make list of readonly ependencies. This template get strings as arguments. Should be added inside System structure.
	*/
	mixin template ReadOnlyDependencies(T...)
	{
		alias ReadOnlyDependencies = T;
	}

	/************************************************************************************************************************
	Make list of writable ependencies. This template get strings as arguments. Should be added inside System structure.
	*/
	mixin template WritableDependencies(T...)
	{
		alias WritableDependencies = T;
	}
}
