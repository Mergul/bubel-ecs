import os
import ntpath
import sys

def compile(sources, output):
    files = []
    # r=root, d=directories, f = files
    for path in sources:
        for r, d, f in os.walk(path):
            for file in f:
                if ntpath.basename(file) != 'win_dll.d':
                    filename, file_extension = os.path.splitext(file)
                    if file_extension == '.d' and filename != 'package':
                        files.append(os.path.join(r, file))
    
    ldc_path = 'ldc'
    if 'LDC' in os.environ:
        ldc_path = os.environ['LDC']
        
    ldc_cmd = ldc_path + ' ' + ldc_flags + '-lib -mtriple=armv7-none-linux-androideabi -fvisibility=hidden -betterC -oq -od=obj/ --singleobj --of=' + output + ' '
    
    for path in sources:
        ldc_cmd += '-I' + path + ' '
        
    for path in import_paths:
        ldc_cmd += '-I' + path + ' '
        
    for f in files:
        ldc_cmd += f + ' '

    print(ldc_cmd)

    if os.system(ldc_cmd):
        exit(0)
    print()

clean = 0
ldc_flags = ''
import_paths = ['source','tests']
build_tests = 0

for arg in sys.argv[1:]:
    if(arg == '-release'):
        ldc_flags += '-release '
    elif(arg == '-enable-inlining'):
        ldc_flags += '-enable-inlining '
    elif(arg == '-O3'):
        ldc_flags += '-O3 '
    elif(arg == '-O2'):
        ldc_flags += '-O2 '
    elif(arg == '-O1'):
        ldc_flags += '-O1 '
    elif(arg == '-O0'):
        ldc_flags += '-O0 '
    elif(arg == '-Os'):
        ldc_flags += '-Os '
    elif(arg == '-Oz'):
        ldc_flags += '-Oz '
    elif(arg == '-g'):
        ldc_flags += '-g '
    elif(arg == '-opt'):
        ldc_flags += '-release -enable-inlining -O3 '
    else:
        print('unknown argument: ' + arg)
        exit()
    
compile(['source'], 'ecs.a')

#export LDC_LIBS=/path/to/your/ldc-build-runtime.tmp/lib/
CC = os.environ['NDK'] + '/toolchains/llvm/prebuilt/linux-x86_64/bin/clang'
TOOLCHAIN = os.environ['NDK'] + '/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64'
SYSROOT = os.environ['NDK'] + '/platforms/android-21/arch-arm'
LDC_LIBS = ''#os.environ['LDC_LIBS'] + '/libphobos2-ldc.a ' + os.environ['LDC_LIBS'] + '/libdruntime-ldc.a'

os.system(CC + ' -Wl,-soname,libecs.so -shared --sysroot=' + SYSROOT + ' obj/*.o ' + LDC_LIBS + ' -lgcc -gcc-toolchain ' + TOOLCHAIN + 
' -no-canonical-prefixes -fuse-ld=bfd -target armv7-none-linux-androideabi -fvisibility=hidden \
-Wl,--gc-sections -Wl,--fix-cortex-a8 -Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro \
-Wl,-z,now -mthumb -o libecs.so')
