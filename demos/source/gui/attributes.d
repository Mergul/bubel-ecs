module gui.attributes;

enum GUIColor = "GUIColor";

struct GUIRange
{
    struct
    {
        int min;
        int max;
    }
}

struct GUIRangeF
{
    struct
    {
        float minf;
        float maxf;
    }
}

enum GUIDisabled = "GUIDisabled";
