module gui.component;

import ecs_utils.utils;

struct ComponentGUI
{
    const (char)* name;
    void* data;
    ushort component_id;
}

struct ComponentEditGUI
{
    const (char)* name;
    VariableGUI[] variables;
    uint used;
}

struct VariableGUI
{
    struct Int 
    {
        int min;
        int max;
    }

    struct Float
    {
        float min;
        float max;
    }

    struct Enum
    {
        const (char)[][] strings;
    }

    enum Type
    {
        byte_,
        ubyte_,
        short_,
        ushort_,
        int_,
        uint_,
        float_,
        enum_,
        color,
        vec2,
        ivec2,
        vec4,
        ivec4
    }
    Type type;
    const (char)* name;
    ushort offset;
    bool disabled = false;
    union 
    {
        Int int_;
        Float float_;
        Enum enum_;
    }
}
