module gui.tool_circle;

import ecs_utils.gfx.buffer;
import ecs_utils.gfx.shader;
import ecs_utils.gfx.config;
import ecs_utils.gfx.renderer;

import ecs_utils.math.vector;

version(WebAssembly)import glad.gl.gles2;
else version(Android)import glad.gl.gles2;
else import glad.gl.gl;

struct ToolCircle
{
    //Buffer vbo;
    //Buffer ibo;

    uint material_id = 1;
    uint mesh_id = 0;

    /*/void generate()
    {
        ushort[]
    }*/

    void draw(Renderer* renderer, vec2 position, float size, float edge = 1)
    {
        glDisable(GL_DEPTH_TEST);
        position = position * renderer.view_size + renderer.view_pos;
        vec2 sizes = renderer.view_size * size;
        vec2 sizes2 = vec2(edge,0);

        import core.stdc.string;
        ubyte[32] uniform_block;
        void* ptr = uniform_block.ptr;
        *cast(float*)(ptr) = sizes.x;
        *cast(float*)(ptr+4) = 0;
        *cast(float*)(ptr+8) = 0;
        *cast(float*)(ptr+12) = sizes.y;
        memcpy(ptr+16,position.data.ptr,8);
        memcpy(ptr+24,sizes2.data.ptr,8);
        glEnableVertexAttribArray(0);

        GfxConfig.meshes[mesh_id].bind();
        GfxConfig.materials[material_id].bind();
        GfxConfig.materials[material_id].pushBindings();
        GfxConfig.materials[material_id].pushUniforms(uniform_block.ptr);
        //glDisable(GL_DEPTH_TEST);

        glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_SHORT,null);
    }
}