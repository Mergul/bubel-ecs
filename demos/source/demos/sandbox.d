module demos.sandbox;

import bindbc.sdl;

import bubel.ecs.core;

import demos.simple;
import demos.snake;
import demos.space_invaders;
import demos.particles;
import demos.brick_breaker;

import game_core.rendering;

import app;

import ecs_utils.math.vector;

extern(C):

void sandboxStart()
{
    simpleRegister();
    snakeRegister();
    spaceInvadersRegister();
    particlesRegister();
    brickBreakerRegister();

    simpleStart();
    snakeStart();
    spaceInvadersStart();
    particlesStart();
    brickBreakerStart();

    DrawSystem* draw_system = gEntityManager.getSystem!DrawSystem;
    draw_system.default_data.size = vec2(16,16);
    draw_system.default_data.coords = vec4(0,48,16,16)*demos.simple.px;
    draw_system.default_data.material_id = 0;
    draw_system.default_data.texture = particles_demo.texture;
    draw_system.default_data.color = 0x80808080;

    gEntityManager.getSystem(becsID!MouseAttractSystem).disable();
    gEntityManager.getSystem(becsID!(demos.simple.MoveSystem)).disable();

}

void sandboxEnd()
{
    simpleEnd();
    snakeEnd();
    spaceInvadersEnd();
    particlesEnd();
}
/*
void sandboxEvent(SDL_Event* event)
{
}*/

bool sandboxLoop()
{
    launcher.render_position = (vec2(launcher.window_size.x,launcher.window_size.y)*launcher.scalling - vec2(400,300)) * 0.5;
    
    gEntityManager.begin();

    float delta_time = launcher.delta_time;
    if(delta_time > 2000)delta_time = 2000;
    __gshared float time = 0;

    /*if(launcher.getKeyState(SDL_SCANCODE_SPACE))time += delta_time * 3;
    else */
    time += delta_time;

    while(time > 200)
    {
        time -= 200;
        
        gEntityManager.update("fixed");
    }

    if(launcher.multithreading)
    {
        launcher.job_updater.begin();
        gEntityManager.updateMT();
        launcher.job_updater.call();
    }
    else
    {
        gEntityManager.update();
    }
    gEntityManager.end();

    return true;
}

DemoCallbacks getSanboxDemo()
{
    DemoCallbacks demo;
    demo.initialize = &sandboxStart;
    demo.deinitialize = &sandboxEnd;
    demo.loop = &sandboxLoop;
    demo.tips = "This demo contains all components, systems and events from previous demos. They was not designed for that kind of coexisting. Some system collide with other making some weird things.
This gives biggest opportunities. You can add tower on top of snake, or lasers to paddle from BrickBreaker. This will be improved in next versions of ECS demo.";
    return demo;
}