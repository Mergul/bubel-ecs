module demos.particles;

import app;

import bindbc.sdl;

import cimgui.cimgui;

import bubel.ecs.attributes;
import bubel.ecs.core;
import bubel.ecs.entity;
import bubel.ecs.manager;
import bubel.ecs.std;

import ecs_utils.gfx.texture;
import ecs_utils.math.vector;
import ecs_utils.utils;

import game_core.basic;
import game_core.rendering;

import gui.attributes;

extern(C):

private enum float px = 1.0/512.0;

/*#######################################################################################################################
------------------------------------------------ Components ------------------------------------------------------------------
#######################################################################################################################*/

/*struct CLocation
{
    mixin ECS.Component;

    alias location this;

    vec2 location;
}

struct CColor
{
    mixin ECS.Component;

    alias value this;

    @GUIColor uint value = uint.max;
}

struct CTexCoords
{
    mixin ECS.Component;

    vec4 value;
}*/

// struct CVelocity
// {
//     mixin ECS.Component;

//     alias value this;

//     vec2 value = vec2(0);
// }

struct CForceRange
{
    mixin ECS.Component;

    vec2 range = vec2(20,200);
}

struct CAttractor
{
    mixin ECS.Component;

    //alias value this;
    float strength = 0.2;
}

struct CVortex
{
    mixin ECS.Component;

    float strength = 0.6;
}

// struct CDamping
// {
//     mixin ECS.Component;

//     alias power this;

//     @GUIRange(0,9) ubyte power = 0;
// }

struct CGravity
{
    mixin ECS.Component;
}

struct CParticleLife
{
    mixin ECS.Component;

    this(float life_in_secs)
    {
        life = cast(int)(life_in_secs * 1000_000);
    }

    alias life this;

    int life = 1000000;
}

/*#######################################################################################################################
------------------------------------------------ Systems ------------------------------------------------------------------
#######################################################################################################################*/

struct MouseAttractSystem
{
    mixin ECS.System!64;

    struct EntitiesData
    {
        uint length;
        @readonly CLocation[] locations;
        CVelocity[] velocity;
    }

    vec2 mouse_pos;

    bool onBegin()
    {
        if(!launcher.getKeyState(SDL_SCANCODE_SPACE))return false;
        mouse_pos = launcher.mouse.position;
        mouse_pos = vec2(mouse_pos.x, mouse_pos.y) * launcher.scalling - launcher.render_position;
        return true;
    }

    void onUpdate(EntitiesData data)
    {
        float speed = launcher.deltaTime * 0.01;
        foreach(i;0..data.length)
        {
            vec2 rel_pos = mouse_pos - data.locations[i];
            float len2 = rel_pos.x * rel_pos.x + rel_pos.y * rel_pos.y;
            if(len2 < 0.1)len2 = 0.1;
            data.velocity[i] = data.velocity[i] + rel_pos / len2 * speed;
        }
    }
}

struct AttractSystem
{
    mixin ECS.System!64;

    struct EntitiesData
    {
        uint length;
        @readonly CLocation[] locations;
        CVelocity[] velocity;
    }

    struct Updater
    {
        AttractSystem.EntitiesData data;

        void onUpdate(AttractorIterator.EntitiesData adata)
        {
            float speed = launcher.deltaTime * 0.00004;
            if(adata.vortex)
            {
                foreach(i;0..data.length)
                {
                    foreach(j;0..adata.length)
                    {
                        vec2 rel_pos = data.locations[i] - adata.locations[j];
                        float len2 = rel_pos.length2();
                        float inv_len = rsqrt(len2);

                        if(1 < adata.force_range[j].range.y*inv_len)
                        {
                            float dist = (adata.force_range[j].range.y - 0.4)*inv_len - 1;
                            
                            vec2 vec = rel_pos * inv_len;
                            vec2 cvec = vec2(-vec.y,vec.x);

                            float sign = -1;
                            if(1 < adata.force_range[j].range.x*inv_len)sign = 1;
                            
                            float str = adata.attractor[j].strength * sign;
                            float vortex_str = adata.vortex[j].strength;
                            data.velocity[i] = data.velocity[i] + (rel_pos * str + cvec * vortex_str) * speed * dist;
                        }
                    }
                }
            }
            else
            {
                foreach(i;0..data.length)
                {
                    foreach(j;0..adata.length)
                    {
                        vec2 rel_pos = data.locations[i] - adata.locations[j];
                        float len2 = rel_pos.length2();
                        float inv_len = rsqrt(len2);

                        if(1 < adata.force_range[j].range.y*inv_len)
                        {
                            float dist = (adata.force_range[j].range.y - 0.4)*inv_len - 1;
                            
                            vec2 vec = rel_pos;

                            float sign = -1;
                            if(1 < adata.force_range[j].range.x*inv_len)sign = 1;
                            
                            float str = adata.attractor[j].strength * speed * dist * sign;
                            data.velocity[i] = data.velocity[i] + vec * str;
                        }
                    }
                }
            }
        }
    }

    void onUpdate(EntitiesData data)
    {
        Updater updater;
        updater.data = data;
        gEntityManager.callEntitiesFunction!AttractorIterator(&updater.onUpdate);
    }
}

struct AttractorIterator
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly CLocation[] locations;
        @readonly CAttractor[] attractor;
        @readonly CForceRange[] force_range;
        @optional @readonly CVortex[] vortex;
    }

    bool onBegin()
    {
        return false;
    }

    void onUpdate(EntitiesData data)
    {
        
    }
}

struct PlayAreaSystem
{
    mixin ECS.System!32;

    struct EntitiesData
    {
        uint length;
        Entity[] entity;
        @readonly CLocation[] locations;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i; 0..data.length)
        {
            if(data.locations[i].x > 440)gEntityManager.removeEntity(data.entity[i].id);
            else if(data.locations[i].x < -40)gEntityManager.removeEntity(data.entity[i].id);
            if(data.locations[i].y > 340)gEntityManager.removeEntity(data.entity[i].id);
            else if(data.locations[i].y < -40)gEntityManager.removeEntity(data.entity[i].id);
        }
    }
}

struct ParticleLifeSystem
{
    mixin ECS.System!32;

    struct EntitiesData
    {
        uint length;
        const (Entity)[] entity;
        CParticleLife[] life;
    }

    int delta_time;

    bool onBegin()
    {
        delta_time = cast(int)(launcher.deltaTime * 1000);
        return true;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i; 0..data.length)
        {
            data.life[i] -= delta_time;
            if(data.life[i] < 0)gEntityManager.removeEntity(data.entity[i].id);
        }
    }
}

struct GravitySystem
{
    mixin ECS.System!32;

    struct EntitiesData
    {
        uint length;
        const (Entity)[] entity;
        @readonly CGravity[] gravity;
        CVelocity[] velocity;
    }

    void onUpdate(EntitiesData data)
    {
        float delta_time = launcher.deltaTime * 0.00_092;
        foreach(i; 0..data.length)
        {
            data.velocity[i].y -= delta_time;
        }
    }
}

/*#######################################################################################################################
------------------------------------------------ Functions ------------------------------------------------------------------
#######################################################################################################################*/

struct ParticlesDemo
{
    __gshared const (char)* tips = "Particles by default have no velocity. You can spawn \"Attractor\" which attract particles, or \"Vortex\" which attracts and spin particles. 
Please do not spawn to many of them as every \"Attractor\" iterate over all particles (brute force).";

    Texture texture;
}

__gshared ParticlesDemo* particles_demo;

void particlesRegister()
{
    particles_demo = Mallocator.make!ParticlesDemo;

    particles_demo.texture.create();
    particles_demo.texture.load("assets/textures/atlas.png");

    gEntityManager.beginRegister();

    registerRenderingModule(gEntityManager);

    gEntityManager.registerComponent!CLocation;
    //gEntityManager.registerComponent!CTexCoords;
    gEntityManager.registerComponent!CColor;
    gEntityManager.registerComponent!CVelocity;
    gEntityManager.registerComponent!CScale;
    gEntityManager.registerComponent!CTexCoords;
    gEntityManager.registerComponent!CTexCoordsIndex;
    gEntityManager.registerComponent!CRotation;
    gEntityManager.registerComponent!CDepth;
    gEntityManager.registerComponent!CAttractor;
    gEntityManager.registerComponent!CDamping;
    gEntityManager.registerComponent!CGravity;
    gEntityManager.registerComponent!CVortex;
    gEntityManager.registerComponent!CParticleLife;
    gEntityManager.registerComponent!CForceRange;
    gEntityManager.registerComponent!CMaterialIndex;
    gEntityManager.registerComponent!CVelocityFactor;

    gEntityManager.registerSystem!MoveSystem(0);
    gEntityManager.registerSystem!DrawSystem(100);
    gEntityManager.registerSystem!PlayAreaSystem(102);
    gEntityManager.registerSystem!AttractSystem(-1);
    gEntityManager.registerSystem!MouseAttractSystem(1);
    gEntityManager.registerSystem!DampingSystem(101);
    gEntityManager.registerSystem!ParticleLifeSystem(-10);
    gEntityManager.registerSystem!GravitySystem(-2);

    gEntityManager.registerSystem!AttractorIterator(-1);

    gEntityManager.endRegister();
}

void particlesStart()
{
    DrawSystem* draw_system = gEntityManager.getSystem!DrawSystem;
    draw_system.default_data.size = vec2(2,2);
    draw_system.default_data.coords = vec4(246,64,2,2)*px;
    draw_system.default_data.material_id = 2;
    draw_system.default_data.texture = particles_demo.texture;

    launcher.gui_manager.addSystem(becsID!MoveSystem,"Move System");
    launcher.gui_manager.addSystem(becsID!DrawSystem,"Draw System");
    launcher.gui_manager.addSystem(becsID!PlayAreaSystem,"Play Area System");
    launcher.gui_manager.addSystem(becsID!AttractSystem,"Attract System");
    launcher.gui_manager.addSystem(becsID!MouseAttractSystem,"Mouse Attract System");
    launcher.gui_manager.addSystem(becsID!DampingSystem,"Damping System");
    launcher.gui_manager.addSystem(becsID!ParticleLifeSystem,"Particle Life System");
    launcher.gui_manager.addSystem(becsID!GravitySystem,"Gravity System");

    // launcher.gui_manager.addComponent(CColor(),"Color (white)");
    // launcher.gui_manager.addComponent(CColor(0xFF101540),"Color (red)");
    // launcher.gui_manager.addComponent(CColor(0xFF251010),"Color (blue)");
    // launcher.gui_manager.addComponent(CColor(0xFF102010),"Color (green)");
    launcher.gui_manager.addComponent(CLocation(),"Location");
    launcher.gui_manager.addComponent(CScale(),"Scale");
    launcher.gui_manager.addComponent(CTexCoords(),"Texture Coords");
    launcher.gui_manager.addComponent(CTexCoordsIndex(),"Texture Coords Index");
    launcher.gui_manager.addComponent(CRotation(),"Rotation");
    launcher.gui_manager.addComponent(CDepth(),"Depth");
    launcher.gui_manager.addComponent(CMaterialIndex(),"Material ID");
    launcher.gui_manager.addComponent(CVelocityFactor(),"Velocity Factor");
    launcher.gui_manager.addComponent(CAttractor(0.1),"Attractor");
    launcher.gui_manager.addComponent(CForceRange(vec2(5,40)),"ForceRange");
    launcher.gui_manager.addComponent(CVelocity(),"Velocity");
    launcher.gui_manager.addComponent(CDamping(),"Damping");
    launcher.gui_manager.addComponent(CVortex(),"Vortex");
    launcher.gui_manager.addComponent(CParticleLife(),"Particle Life");
    launcher.gui_manager.addComponent(CGravity(),"Gravity");

    EntityTemplate* tmpl;
    EntityTemplate* base_tmpl = gEntityManager.allocateTemplate([becsID!CTexCoords, becsID!CLocation, becsID!CColor, becsID!CVelocity, becsID!CDamping, becsID!CScale, becsID!CMaterialIndex].staticArray);
    base_tmpl.getComponent!CColor().value = 0xFF251010;
    base_tmpl.getComponent!CScale().value = vec2(2);
    base_tmpl.getComponent!CTexCoords().value = vec4(246,64,2,2)*px;
    base_tmpl.getComponent!CMaterialIndex().value = 2;
    launcher.gui_manager.addTemplate(base_tmpl,"Particle");
    // tmpl = gEntityManager.allocateTemplate(base_tmpl);
    // tmpl.getComponent!CColor().value = 0xFF251010;
    // launcher.gui_manager.addTemplate(tmpl,"Particle (blue)");
    // tmpl = gEntityManager.allocateTemplate(base_tmpl);
    // tmpl.getComponent!CColor().value = 0xFF102010;
    // launcher.gui_manager.addTemplate(tmpl,"Particle (green)");
    // tmpl = gEntityManager.allocateTemplate(base_tmpl);
    // tmpl.getComponent!CColor().value = 0xFF101540;
    // launcher.gui_manager.addTemplate(tmpl,"Particle (red)");
    // tmpl = gEntityManager.allocateTemplate(tmpl, [becsID!CDamping].staticArray);
    // launcher.gui_manager.addTemplate(tmpl,"Particle (damping)");
    // tmpl = gEntityManager.allocateTemplate(tmpl);
    // tmpl.getComponent!CDamping().power = 4;
    // launcher.gui_manager.addTemplate(tmpl,"Particle (damping!)");
    tmpl = gEntityManager.allocateTemplate([becsID!CAttractor, becsID!CLocation, becsID!CForceRange, becsID!CScale].staticArray);
    tmpl.getComponent!CScale().value = vec2(4);
    launcher.gui_manager.addTemplate(tmpl,"Attractor");
    tmpl = gEntityManager.allocateTemplate(tmpl, [becsID!CVortex].staticArray);
    launcher.gui_manager.addTemplate(tmpl,"Vortex");
    // tmpl = gEntityManager.allocateTemplate(tmpl);
    // tmpl.getComponent!CVortex().strength = -0.6;
    // launcher.gui_manager.addTemplate(tmpl,"Vortex (reversed)");

}

void particlesEnd()
{
    particles_demo.texture.destroy();

    //gEntityManager.freeTemplate(simple.tmpl);
    Mallocator.dispose(particles_demo);
}

void particlesEvent(SDL_Event* event)
{
}

bool particlesLoop()
{
    launcher.render_position = (vec2(launcher.window_size.x,launcher.window_size.y)*launcher.scalling - vec2(400,300)) * 0.5;
    
    gEntityManager.begin();
    if(launcher.multithreading)
    {
        launcher.job_updater.begin();
        gEntityManager.updateMT();
        launcher.job_updater.call();
    }
    else
    {
        gEntityManager.update();
    }
    gEntityManager.end();

    return true;
}

DemoCallbacks getParticlesDemo()
{
    DemoCallbacks demo;
    demo.register = &particlesRegister;
    demo.initialize = &particlesStart;
    demo.deinitialize = &particlesEnd;
    demo.loop = &particlesLoop;
    demo.tips = ParticlesDemo.tips;
    return demo;
}