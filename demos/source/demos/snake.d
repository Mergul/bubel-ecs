module demos.snake;

import app;

import bindbc.sdl;

import bubel.ecs.attributes;
import bubel.ecs.core;
import bubel.ecs.entity;
import bubel.ecs.manager;
import bubel.ecs.std;
import bubel.ecs.vector;

import cimgui.cimgui;

import ecs_utils.gfx.texture;
import ecs_utils.math.vector;
import ecs_utils.utils;

import game_core.basic;

import gui.attributes;
//import std.array : staticArray;

enum float px = 1.0/512.0;

extern(C):

//Map is simple grid. Every cell has type and id to entity.
struct MapElement
{
    enum Type
    {
        empty = 0,
        apple = 1,
        wall = 2,
        snake = 3,
    }
    Type type;
    EntityID id;
}

//snake part is corresponding to graphical representation of snake
enum SnakePart : ubyte
{
    head_up = 0,
    head_down = 1,
    head_left = 2,
    head_right = 3,
    tail_up = 4,
    tail_down = 5,
    tail_left = 6,
    tail_right = 7,
    turn_ld = 8,
    turn_lu = 9,
    turn_rd = 10,
    turn_ru = 11,
    vertical = 12,
    horizontal = 13
}

struct Snake
{
    __gshared const (char)* tips = "Use \"WASD\" keys to move. If you loose you can always spawn new snake... or several snakes.
This demo is an example that in ECS you can make very \"non-ECS\" game";

    EntityTemplate* apple_tmpl;
    EntityTemplate* snake_tmpl;
    EntityTemplate* snake_destroy_particle;
    Texture texture;

    vec4[] snake_destroy_particle_frames;
    vec4[] smoke_frames;


    bool move_system = true;
    bool draw_system = true;

    enum int map_size = 18;

    MapElement[map_size * map_size] map;

    ~this() @nogc nothrow
    {
        if(snake_destroy_particle_frames)Mallocator.dispose(snake_destroy_particle_frames);
        if(smoke_frames)Mallocator.dispose(smoke_frames);
        if(apple_tmpl)gEntityManager.freeTemplate(apple_tmpl);
        if(snake_tmpl)gEntityManager.freeTemplate(snake_tmpl);
        if(snake_destroy_particle)gEntityManager.freeTemplate(snake_destroy_particle);
        texture.destroy();
    }

    MapElement element(ivec2 pos)
    {
        uint index = pos.x + pos.y * map_size;
        if(index >= map.length)index = map.length - 1;
        return map[index];
    }

    void element(MapElement el, ivec2 pos)
    {
        uint index = pos.x + pos.y * map_size;
        if(index >= map.length)index = map.length - 1;
        map[index] = el;
    }

    void addApple()
    {
        ivec2 random_pos = ivec2(rand()%map_size,rand()%map_size);
        ivec2 base_pos = random_pos;
        while(element(random_pos).type != MapElement.Type.empty)
        {
            random_pos.x += 1;
            if(random_pos.x > map_size)
            {
                random_pos.x = 0;
                random_pos.y += 1;
                if(random_pos.y > map_size)random_pos.y = 0;
            }
            if(base_pos.x == random_pos.x && base_pos.y == random_pos.y)return;
        }
        gEntityManager.addEntity(apple_tmpl,[CLocation(cast(vec2)(random_pos)*16).ref_].staticArray);
    }
}

struct Animation
{

}

//component has array of frames (texture coordinates) and time used for selection frames
struct CAnimation
{
    mixin ECS.Component;
    
    vec4[] frames;
    @GUIRangeF(0,float.max)float time = 0;
}

//CIlocation is integer location used as grid cell coordination
struct CILocation
{
    mixin ECS.Component;

    alias location this;

    ivec2 location;
}

// struct CLocation
// {
//     mixin ECS.Component;

//     alias location this;

//     vec2 location = vec2(0,0);
// }

struct CSnake
{
    void onCreate()
    {
        parts.array = Mallocator.makeArray!ivec2(100);
    }

    void onDestroy()
    {
        Mallocator.dispose(parts.array);
    }

    mixin ECS.Component;


    struct Parts
    {
        uint length = 0;
        ivec2[] array;

        ivec2 opIndex(size_t ind) const
        {
            return array[ind];
        }

        ivec2[] opSlice() 
        {
            return array[0 .. length];
        }

        void opIndexAssign(ivec2 vec, size_t ind) 
        {
            array[ind] = vec;
        }

        size_t opDollar() const 
        {
            return length;
        }

        void add(ivec2 v)
        {
            length++;
            array[length-1] = v;
        }
    }

    Parts parts;
    @GUIRange(0,3)CMovement.Direction direction;
}

//flag for apple
struct CApple
{
    mixin ECS.Component;
}

//particle is removed when life drops below 0
struct CParticle
{
    mixin ECS.Component;

    float life = 0;
}

//vector for particle movement
struct CParticleVector
{
    mixin ECS.Component;

    vec2 velocity = vec2(0,0);
}

//contains current movement direction for snake
struct CMovement
{
    mixin ECS.Component;

    enum Direction : byte
    {
        up,
        down,
        left,
        right
    }   

    @GUIRange(0,3)Direction direction;
}

// struct CInput
// {
//     mixin ECS.Component;
// }

//this system has no onUpdate and only processing events. It responsible for adding and removing applce from grid.
struct AppleSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly Entity[] entity;
        @readonly CApple[] apple;
        @readonly CILocation[] location;
    }

    //called when entity was created
    void onAddEntity(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            if(snake.element(data.location[i]).id == EntityID())snake.element(MapElement(MapElement.Type.apple,data.entity[i].id),data.location[i]);
            else gEntityManager.removeEntity(data.entity[i].id);
        }
    }

    //called when entity was removed
    void onRemoveEntity(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            if(snake.element(data.location[i].location).id == data.entity[i].id)
                snake.element(MapElement(MapElement.Type.empty, EntityID()),data.location[i].location);
        }
    }
}

//system is responsible for killing particles when their life drops below 0
struct ParticleSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly Entity[] entities;
        CParticle[] particle;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            data.particle[i].life -= launcher.deltaTime;
            if(data.particle[i].life < 0)gEntityManager.removeEntity(data.entities[i].id);
        }
    }
}

//system responsible for moving particles
struct ParticleMovementSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly Entity[] entities;
        @readonly CParticleVector[] movement;
        CLocation[] location;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            data.location[i] -= data.movement[i].velocity;
        }
    }
}


struct AnimationSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        CAnimation[] animation;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            data.animation[i].time += launcher.deltaTime * 0.01;
            while(data.animation[i].time >= data.animation[i].frames.length)data.animation[i].time -= cast(float)data.animation[i].frames.length;
        }
    }
}


struct AnimationRenderSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly CAnimation[] animation;
        @readonly CLocation[] location;
    }

    void onUpdate(EntitiesData data)
    {
        import ecs_utils.gfx.renderer;
        Renderer.DrawData draw_data;
        draw_data.size = vec2(16,16);
        //draw_data.coords = vec4(0,0,1,1)*px;
        draw_data.color = 0x80808080;
        draw_data.material_id = 0;
        draw_data.thread_id = 0;
        draw_data.texture = snake.texture;
        draw_data.depth = -1;
        foreach(i;0..data.length)
        {
            uint frame = cast(uint)(data.animation[i].time);
            if(frame >= data.animation[i].frames.length)frame = cast(uint)data.animation[i].frames.length - 1;
            draw_data.position = data.location[i];
            draw_data.coords = data.animation[i].frames[frame];
            launcher.renderer.draw(draw_data);
        }
    }
}

struct MoveSystem
{
    mixin ECS.System!64;

    EntityTemplate* destroy_template;

    struct EntitiesData
    {
        uint length;
        @readonly Entity[] entities;
        @readonly CMovement[] movement;
        @optional CSnake[] snakes;
        CILocation[] location;
    }

    void setTemplates()
    {
        //template is used for adding particles when snake will collide with himself
        destroy_template = snake.snake_destroy_particle;
    }

    void moveLocation(ref CILocation location, CMovement.Direction direction)
    {
        final switch(direction)
        {
            case CMovement.Direction.down:
                location.y -= 1;
                if(location.y < 0)location.y = snake.map_size - 1;
                break;
            case CMovement.Direction.up:
                location.y += 1;
                if(location.y >= snake.map_size)location.y = 0;
                break;
            case CMovement.Direction.left:
                location.x -= 1;
                if(location.x < 0)location.x = snake.map_size - 1;
                break;
            case CMovement.Direction.right:
                location.x += 1;
                if(location.x >= snake.map_size)location.x = 0;
                break;
        }
    }

    void moveSnake(ref CSnake snake, ivec2 location)
    {
        if(snake.parts.length)
        {
            .snake.element(MapElement(),snake.parts[0]);
            foreach(j; 0 .. snake.parts.length - 1)
            {
                snake.parts[j] = snake.parts[j + 1];
            }
            snake.parts[$-1] = location;
        }
        else .snake.element(MapElement(),location);
    }

    void onUpdate(EntitiesData data)
    {
        if(data.snakes)
        {
            foreach(i; 0..data.length)
            {
                data.snakes[i].direction = data.movement[i].direction;
                ivec2 new_location = data.location[i];
                moveLocation(data.location[i], data.movement[i].direction);
                final switch(snake.element(data.location[i].location).type)
                {
                    case MapElement.Type.snake:
                        foreach(loc; data.snakes[i].parts)
                        {
                            //destroy_location.x = loc.x * 16;
                            //destroy_location.y = loc.y * 16;
                            snake.element(MapElement(MapElement.Type.empty, EntityID()),loc);
                            gEntityManager.addEntity(snake.snake_destroy_particle,[CLocation(cast(vec2)(loc * 16)).ref_].staticArray);

                            CLocation destroy_location;
                            foreach(j;0..10)
                            {
                                destroy_location.x = loc.x * 16 + randomf() * 8 - 4;
                                destroy_location.y = loc.y * 16 + randomf() * 8 - 4;
                                //destroy_vector.velocity = vec2(randomf(),randomf())*0.4-0.2;
                                snake.element(MapElement(MapElement.Type.empty, EntityID()),loc);
                                gEntityManager.addEntity(snake.snake_destroy_particle, [destroy_location.ref_, CParticleVector(vec2(randomf(),randomf())*0.4-0.2).ref_].staticArray);
                            }
                            
                        }
                        //destroy_location.x = new_location.x * 16;
                        //destroy_location.y = new_location.y * 16;
                        snake.element(MapElement(MapElement.Type.empty, EntityID()),new_location);
                        gEntityManager.addEntity(snake.snake_destroy_particle,[CLocation(cast(vec2)(new_location * 16)).ref_].staticArray);
                        gEntityManager.removeEntity(data.entities[i].id);
                        break;

                    case MapElement.Type.wall:break;

                    case MapElement.Type.empty:
                        moveSnake(data.snakes[i], new_location);
                        snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.location[i].location);
                        if(data.snakes[i].parts.length > 1)
                        {
                            snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.snakes[i].parts[$-1]);
                            snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.snakes[i].parts[0]);
                        }
                        else if(data.snakes[i].parts.length == 1)
                        {
                            snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.snakes[i].parts[0]);
                        }
                        break;
                    case MapElement.Type.apple:
                        gEntityManager.removeEntity(snake.element(data.location[i].location).id);
                        if(data.snakes[i].parts.length >= 99)
                        {
                            snake.addApple();
                            goto case(MapElement.Type.empty);
                        }
                        data.snakes[i].parts.add(new_location);

                        if(data.snakes[i].parts.length > 1)
                        {
                            snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.snakes[i].parts[$-1]);
                        }
                        else if(data.snakes[i].parts.length == 1)
                        {
                            snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),new_location);
                        }
                        snake.element(MapElement(MapElement.Type.snake, data.entities[i].id),data.location[i].location);
                        snake.addApple();
                        break;
                }
            }
        }
        else
        {
            foreach(i; 0..data.length)
            {
                final switch(data.movement[i].direction)
                {
                    case CMovement.Direction.down:data.location[i].y -= 1;break;
                    case CMovement.Direction.up:data.location[i].y += 1;break;
                    case CMovement.Direction.left:data.location[i].x -= 1;break;
                    case CMovement.Direction.right:data.location[i].x += 1;break;
                }
            }
        }
    }
}

struct SnakeSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        Entity[] entity;
        @readonly CSnake[] snake;
        @readonly CILocation[] location;
    }

    void onAddSystem(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            if(snake.element(data.location[i]).id == EntityID())snake.element(MapElement(MapElement.Type.snake,data.entity[i].id),data.location[i]);
            else gEntityManager.removeEntity(data.entity[i].id);
        }
    }

    void onRemoveEntity(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            if(snake.element(data.location[i].location).id == data.entity[i].id)
                snake.element(MapElement(MapElement.Type.empty, EntityID()),data.location[i].location);
            foreach(part; data.snake[i].parts.array)
                if(snake.element(part).id == data.entity[i].id)
                    snake.element(MapElement(MapElement.Type.empty, EntityID()),part);
        }
    }
}

struct InputSystem
{
    mixin ECS.System!64;

    struct EntitiesData
    {
        uint length;
        CMovement[] movement;
        @readonly CInput[] input;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i; 0..data.length)
        {
            if(launcher.getKeyState(SDL_SCANCODE_W))
            {
                data.movement[i].direction = CMovement.Direction.up;
            }
            else if(launcher.getKeyState(SDL_SCANCODE_S))
            {
                data.movement[i].direction = CMovement.Direction.down;
            }
            else if(launcher.getKeyState(SDL_SCANCODE_A))
            {
                data.movement[i].direction = CMovement.Direction.left;
            }
            else if(launcher.getKeyState(SDL_SCANCODE_D))
            {
                data.movement[i].direction = CMovement.Direction.right;
            }
        }
    }
}

struct FixSnakeDirectionSystem
{
    mixin ECS.System!64;

    struct EntitiesData
    {
        uint length;
        CMovement[] movement;
        @readonly CILocation[] location;
        const (CSnake)[] snake;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i; 0..data.length)
        {
            ivec2 last_location;
            if(data.snake[i].parts.length)last_location = data.snake[i].parts[$ - 1];
            else continue;
            ivec2 next_location = data.location[i];
            
            final switch(data.movement[i].direction)
            {
                case CMovement.Direction.up:
                    next_location.y += 1;
                    if(next_location.y >= snake.map_size)next_location.y = 0;
                    if(next_location.x == last_location.x && next_location.y == last_location.y)
                    {
                        data.movement[i].direction = CMovement.Direction.down;
                    }
                    break;
                case CMovement.Direction.down:
                    next_location.y -= 1;
                    if(next_location.y < 0)next_location.y = snake.map_size - 1;
                    if(next_location.x == last_location.x && next_location.y == last_location.y)
                    {
                        data.movement[i].direction = CMovement.Direction.up;
                    }
                    break;
                case CMovement.Direction.left:
                    next_location.x -= 1;
                    if(next_location.x < 0)next_location.x = snake.map_size - 1;
                    if(next_location.x == last_location.x && next_location.y == last_location.y)
                    {
                        data.movement[i].direction = CMovement.Direction.right;
                    }
                    break;
                case CMovement.Direction.right:
                    next_location.x += 1;
                    if(next_location.x >= snake.map_size)next_location.x = 0;
                    if(next_location.x == last_location.x && next_location.y == last_location.y)
                    {
                        data.movement[i].direction = CMovement.Direction.left;
                    }
                    break;
            }
        }
    }
}

struct DrawAppleSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly CILocation[] location;
        const (CApple)[] apple;
    }

    void onUpdate(EntitiesData data)
    {
        import ecs_utils.gfx.renderer;
        Renderer.DrawData draw_data;
        draw_data.size = vec2(16,16);
        draw_data.coords = vec4(0,32*px,16*px,16*px);
        draw_data.color = 0x80808080;
        draw_data.material_id = 0;
        draw_data.thread_id = 0;
        draw_data.texture = snake.texture;
        foreach(i; 0..data.location.length)
        {
            draw_data.position = vec2(data.location[i].x*16,data.location[i].y*16);
            launcher.renderer.draw(draw_data);
            //launcher.renderer.draw(snake.texture, vec2(data.location[i].x*16,data.location[i].y*16), vec2(16,16), vec4(0,32*px,16*px,16*px), 0, 0x80808080, 0);
        }
    }
}

struct DrawSnakeSystem
{
    mixin ECS.System!1;

    struct EntitiesData
    {
        uint length;
        @readonly CILocation[] location;
        const (CSnake)[] snake;
    }

    static CMovement.Direction getDirection(ivec2 p1, ivec2 p2)
    {
        if(p1.x - p2.x == -1)return CMovement.direction.right;
        else if(p1.x - p2.x == 1)return CMovement.direction.left;
        else if(p1.y - p2.y == -1)return CMovement.direction.up;
        else if(p1.y - p2.y == 1)return CMovement.direction.down;
        else if(p1.x - p2.x > 1)return CMovement.direction.right;
        else if(p1.x - p2.x < -1)return CMovement.direction.left;
        else if(p1.y - p2.y > 1)return CMovement.direction.up;
        else return CMovement.direction.down;
    }

    static SnakePart snakePart(ivec2 p1, ivec2 p2, ivec2 p3)
    {
        CMovement.Direction direction = getDirection(p1, p2);
        CMovement.Direction direction2 = getDirection(p1, p3);
        uint case_ = direction*4 + direction2;
        final switch(case_)
        {
            case 0:return SnakePart.horizontal;
            case 1:return SnakePart.horizontal;
            case 2:return SnakePart.turn_lu;
            case 3:return SnakePart.turn_ru;
            case 4:return SnakePart.horizontal;
            case 5:return SnakePart.horizontal;
            case 6:return SnakePart.turn_ld;
            case 7:return SnakePart.turn_rd;
            case 8:return SnakePart.turn_lu;
            case 9:return SnakePart.turn_ld;
            case 10:return SnakePart.vertical;
            case 11:return SnakePart.vertical;
            case 12:return SnakePart.turn_ru;
            case 13:return SnakePart.turn_rd;
            case 14:return SnakePart.vertical;
            case 15:return SnakePart.vertical;
        }
    }

    static SnakePart snakeTail(ivec2 p1, ivec2 p2)
    {
        CMovement.Direction direction = getDirection(p1, p2);
        final switch(direction)
        {
            case CMovement.Direction.up:return SnakePart.tail_up;
            case CMovement.Direction.down:return SnakePart.tail_down;
            case CMovement.Direction.left:return SnakePart.tail_left;
            case CMovement.Direction.right:return SnakePart.tail_right;
        }
    }

    static void drawElement(ivec2 loc, SnakePart part)
    {
        import ecs_utils.gfx.renderer;
        Renderer.DrawData draw_data;
        draw_data.size = vec2(16,16);
        draw_data.color = 0x80808080;
        draw_data.texture = snake.texture;
        draw_data.position = cast(vec2)loc;
        final switch(cast(ubyte)part)
        {
            case SnakePart.tail_up:draw_data.coords = vec4(16,112,16,16)*px;break;
            case SnakePart.tail_down:draw_data.coords = vec4(0,112,16,16)*px;break;
            case SnakePart.tail_left:draw_data.coords = vec4(32,112,16,16)*px;break;
            case SnakePart.tail_right:draw_data.coords = vec4(0,144,16,16)*px;break;
            case SnakePart.turn_ld:draw_data.coords = vec4(64,128,16,16)*px;break;
            case SnakePart.turn_lu:draw_data.coords = vec4(32,144,16,16)*px;break;
            case SnakePart.turn_rd:draw_data.coords = vec4(16,144,16,16)*px;break;
            case SnakePart.turn_ru:draw_data.coords = vec4(64,112,16,16)*px;break;
            case SnakePart.vertical:draw_data.coords = vec4(16,128,16,16)*px;break;
            case SnakePart.horizontal:draw_data.coords = vec4(48,128,16,16)*px;break;
        }
        launcher.renderer.draw(draw_data);
    }

    void onUpdate(EntitiesData data)
    {
        import ecs_utils.gfx.renderer;
        Renderer.DrawData draw_data;
        draw_data.size = vec2(16,16);
        draw_data.color = 0x80808080;
        draw_data.texture = snake.texture;
        
        foreach(i; 0..data.length)
        {
            const (CSnake)* snake = &data.snake[i];
            scope vec2 loc = cast(vec2)(data.location[i].location * 16);
            draw_data.position = loc;
            final switch(snake.direction)
            {
                case CMovement.Direction.up:draw_data.coords = vec4(48,112,16,16)*px;break;
                case CMovement.Direction.down:draw_data.coords = vec4(48,144,16,16)*px;break;
                case CMovement.Direction.left:draw_data.coords = vec4(0,128,16,16)*px;break;
                case CMovement.Direction.right:draw_data.coords = vec4(32,128,16,16)*px;break;
            }
            launcher.renderer.draw(draw_data);
            if(snake.parts.length >1)
            {
                foreach(j;1..snake.parts.length - 1)drawElement(snake.parts[j]*16, snakePart(snake.parts[j], snake.parts[j+1], snake.parts[j-1]));
                drawElement(snake.parts[$-1]*16, snakePart(snake.parts[$-1], data.location[i], snake.parts[$-2]));
                drawElement(snake.parts[0]*16, snakeTail(snake.parts[1], snake.parts[0]));
            }
            else if(snake.parts.length == 1)
            {
                drawElement(snake.parts[0]*16, snakeTail(data.location[i], snake.parts[0]));
            }
            
        }
    }
}

struct CleanSystem
{
    mixin ECS.System!64;

    struct EntitiesData
    {
        uint length;
        Entity[] entities;
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i; 0..data.length)
        {
            gEntityManager.removeEntity(data.entities[i].id);
        }
    }
}

struct CopyLocationSystem
{
    mixin ECS.System!32;

    struct EntitiesData
    {
        uint length;
        const (Entity)[] entity;
        CLocation[] location;
        @readonly CILocation[] ilocation;
    }

    void onAddEntity(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            data.ilocation[i] = cast(ivec2)(data.location[i] / 16);
        }
    }

    void onUpdate(EntitiesData data)
    {
        foreach(i;0..data.length)
        {
            data.location[i] = cast(vec2)(data.ilocation[i] * 16);
        }
    }
}

__gshared Snake* snake;

void snakeRegister()
{
    import game_core.rendering;

    snake = Mallocator.make!Snake;

    snake.texture.create();
    snake.texture.load("assets/textures/atlas.png");

    gEntityManager.beginRegister();

    gEntityManager.registerPass("fixed");

    registerRenderingModule(gEntityManager);

    gEntityManager.registerComponent!CLocation;
    gEntityManager.registerComponent!CILocation;
    gEntityManager.registerComponent!CSnake;
    gEntityManager.registerComponent!CApple;
    gEntityManager.registerComponent!CParticle;
    gEntityManager.registerComponent!CParticleVector;
    gEntityManager.registerComponent!CMovement;
    gEntityManager.registerComponent!CInput;
    gEntityManager.registerComponent!CAnimation;

    gEntityManager.registerSystem!MoveSystem(0,"fixed");
    gEntityManager.registerSystem!InputSystem(-100);
    gEntityManager.registerSystem!FixSnakeDirectionSystem(-1,"fixed");
    gEntityManager.registerSystem!AnimationRenderSystem(100);
    gEntityManager.registerSystem!AnimationSystem(-1);
    gEntityManager.registerSystem!ParticleSystem(-1);
    gEntityManager.registerSystem!ParticleMovementSystem(-1);
    gEntityManager.registerSystem!DrawAppleSystem(99);
    gEntityManager.registerSystem!DrawSnakeSystem(101);

    gEntityManager.registerSystem!CopyLocationSystem(100);
    //gEntityManager.registerSystem!AppleRemoveSystem(100);
    gEntityManager.registerSystem!AppleSystem(101);
    gEntityManager.registerSystem!SnakeSystem(101);

    gEntityManager.endRegister();
}

void snakeStart()
{
    launcher.gui_manager.addComponent(CApple(),"Apple");
    launcher.gui_manager.addComponent(CSnake(),"Snake");
    launcher.gui_manager.addComponent(CParticle(1000),"Particle");
    launcher.gui_manager.addComponent(CParticleVector(vec2(0,1)),"Particle Vector");
    launcher.gui_manager.addComponent(CInput(),"Input");
    launcher.gui_manager.addComponent(CMovement(CMovement.Direction.up),"Movement");
    launcher.gui_manager.addComponent(CAnimation(),"Animation");
    launcher.gui_manager.addComponent(CILocation(),"Int Location");
    launcher.gui_manager.addComponent(CLocation(),"Location");

    launcher.gui_manager.addSystem(becsID!MoveSystem,"Move System");
    launcher.gui_manager.addSystem(becsID!InputSystem,"Input System");
    launcher.gui_manager.addSystem(becsID!FixSnakeDirectionSystem,"Fix Direction System");
    launcher.gui_manager.addSystem(becsID!AnimationRenderSystem,"Animation Render System");
    launcher.gui_manager.addSystem(becsID!AnimationSystem,"Animation System");
    launcher.gui_manager.addSystem(becsID!ParticleSystem,"Particle Life System");
    launcher.gui_manager.addSystem(becsID!ParticleMovementSystem,"Particle Movement System");
    launcher.gui_manager.addSystem(becsID!DrawAppleSystem,"Draw Apple System");
    launcher.gui_manager.addSystem(becsID!DrawSnakeSystem,"Draw Snake System");
    launcher.gui_manager.addSystem(becsID!CopyLocationSystem,"Copy Location System");
    //launcher.gui_manager.addSystem(becsID!AppleSystem,"Apple System");
    //launcher.gui_manager.addSystem(becsID!SnakeSystem,"Snake System");

    snake.snake_destroy_particle_frames = Mallocator.makeArray([vec4(64,144,16,16)*px,vec4(80,144,16,16)*px,vec4(96,144,16,16)*px,vec4(112,144,16,16)*px].staticArray);

    {
        ushort[5] components = [becsID!CILocation, becsID!CSnake, becsID!CMovement, becsID!CInput, becsID!CLocation];
        snake.snake_tmpl = gEntityManager.allocateTemplate(components);
        gEntityManager.addEntity(snake.snake_tmpl,[CILocation(ivec2(2,2)).ref_].staticArray);
    }

    {
        snake.snake_destroy_particle = gEntityManager.allocateTemplate([becsID!CLocation, becsID!CParticle, becsID!CParticleVector, becsID!CAnimation, becsID!CLocation].staticArray);
        CAnimation* canim = snake.snake_destroy_particle.getComponent!CAnimation;
        canim.frames = snake.snake_destroy_particle_frames;
        CParticle* particle = snake.snake_destroy_particle.getComponent!CParticle;
        particle.life = 400;
    }

    {
        ushort[3] components = [becsID!CILocation, becsID!CApple, becsID!CLocation];
        snake.apple_tmpl = gEntityManager.allocateTemplate(components);
        snake.addApple();
    }
    
    launcher.gui_manager.addTemplate(gEntityManager.allocateTemplate(snake.snake_tmpl), "Snake");
    launcher.gui_manager.addTemplate(gEntityManager.allocateTemplate(snake.apple_tmpl), "Apple");
    launcher.gui_manager.addTemplate(gEntityManager.allocateTemplate(snake.snake_destroy_particle), "Particle");

    MoveSystem* move_system = gEntityManager.getSystem!MoveSystem();
    move_system.setTemplates();
}

void snakeEnd()
{
    //gEntityManager.freeTemplate(simple.tmpl);
    Mallocator.dispose(snake);
}

void snakeEvent(SDL_Event* event)
{

}

bool snakeLoop()
{
    launcher.render_position = (vec2(launcher.window_size.x,launcher.window_size.y)*launcher.scalling - vec2(288,288)) * 0.5;

    // if(launcher.show_demo_wnd)
    // {
    //     igSetNextWindowPos(ImVec2(800 - 260, 30), ImGuiCond_Once, ImVec2(0,0));
    //     igSetNextWindowSize(ImVec2(250, 0), ImGuiCond_Once);
    //     if(igBegin("Snake",&launcher.show_demo_wnd,0))
    //     {
            
    //     }
    //     igEnd();
    // }

    gEntityManager.begin();

    float delta_time = launcher.deltaTime;
    if(delta_time > 2000)delta_time = 2000;
    __gshared float time = 0;

    if(launcher.getKeyState(SDL_SCANCODE_SPACE))time += delta_time * 3;
    else time += delta_time;
    
    while(time > 200)
    {
        time -= 200;
        
        gEntityManager.update("fixed");
    }

    gEntityManager.update();

    gEntityManager.end();

    return true;
}

DemoCallbacks getSnakeDemo()
{
    DemoCallbacks demo;
    demo.register = &snakeRegister;
    demo.initialize = &snakeStart;
    demo.deinitialize = &snakeEnd;
    demo.loop = &snakeLoop;
    demo.tips = snake.tips;
    return demo;
}