import os
import ntpath
import sys
import imp

def compile(sources, output):
    files = []
    # r=root, d=directories, f = files
    for path in sources:
        for r, d, f in os.walk(path):
            for file in f:
                if ntpath.basename(file) != 'win_dll.d':
                    filename, file_extension = os.path.splitext(file)
                    if file_extension == '.d' and filename != 'package':
                        files.append(os.path.join(r, file))

    ldc_cmd = compiler + shared_flags + ldc_flags + '-oq -mtriple=wasm32-unknown-unknown-wasm -betterC --output-bc --od=.bc --singleobj --checkaction=C --of=' + output + ' '

    for path in sources:
        ldc_cmd += '-I' + path + ' '
        
    for path in import_paths:
        ldc_cmd += '-I' + path + ' '
        
    for f in files:
        ldc_cmd += f + ' '

    print(ldc_cmd)

    if os.system(ldc_cmd):
        exit(0)
    print()


compiler = 'ldc2 '
#compiler = 'ldmd2 -vtls '
shared_flags = ''
clean = 0
demo = 0
only_bc = 0
multi = 0
sources = ['tests', 'source']
emc_flags = '-s USE_SDL=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS="[\'png\']" '
ldc_flags = '--d-version=ECSEmscripten --d-version=SDL_209 --d-version=BindSDL_Static --d-version=BindSDL_Image --d-version=MM_USE_POSIX_THREADS '
import_paths = ['external/sources', 'external/imports', 'external/wasm_imports', '../source', 'utils/source', 'simple/source']

for arg in sys.argv[1:]:
    if(arg == '-release'):
        ldc_flags += '-release '
    elif(arg == '-enable-inlining'):
        ldc_flags += '-enable-inlining '
    elif(arg == '-O3'):
        shared_flags += '-O3 '
    elif(arg == '-O2'):
        shared_flags += '-O2 '
    elif(arg == '-O1'):
        shared_flags += '-O1 '
    elif(arg == '-O0'):
        shared_flags += '-O0 '
    elif(arg == '-Os'):
        shared_flags += '-Os '
    elif(arg == '-Oz'):
        shared_flags += '-Oz '
    elif(arg == '-g'):
        shared_flags += '-g '
    elif(arg == '--multi'):
        multi = 1
    elif(arg == '-g4'):
        ldc_flags += '-g '
        emc_flags += '-g4 --source-map-base ./ '
    elif(arg == '--llvm-lto'):
        emc_flags += '--llvm-lto 3 '
    elif(arg == '--simd'):
        emc_flags += '-s SIMD=1 '
    elif(arg == '-opt'):
        shared_flags += '-O3 '
        ldc_flags += '-release -enable-inlining '
        emc_flags += '--llvm-lto 3 -s SIMD=1 '
    elif(arg == '-quiet'):
        emc_flags += "-Wl,--no-check-features "
    elif(arg == '--clean'):
        clean = 1
    elif(arg == '-pthread'):
        emc_flags += '-s USE_PTHREADS=1 '
    elif(arg == '--demo=simple'):
        demo = 0
    elif(arg == '--only-bc'):
        only_bc = 1
    else:
        print('unknown argument: ' + arg)
        exit()
    
compile(['external/wasm_imports/bindbc/sdl'], 'bindbc-sdl.bc')
compile(['utils/source'], 'utils.bc')
compile(['external/sources/mmutils'], 'mmutils.bc')
compile(['external/sources/glad'], 'glad.bc')
compile(['source'], 'demo.bc')

if clean or os.path.exists('../ecs.bc') == 0 or os.path.isfile('../ecs.bc') == 0:
    compile(['../source'], '../ecs.bc')
  
if only_bc:
    exit()
  
if multi:
    emcc_cmd = 'emcc ' + shared_flags + emc_flags + '--pre-js build/assets.js -s FORCE_FILESYSTEM=1 -s MAX_WEBGL_VERSION=2 --emrun -s ERROR_ON_UNDEFINED_SYMBOLS=0 -s DISABLE_DEPRECATED_FIND_EVENT_TARGET_BEHAVIOR=1 -s ALLOW_MEMORY_GROWTH=1 -s WASM_MEM_MAX=2048MB -s MALLOC=dlmalloc -s WASM=1 {0} -o {1} --shell-file emscripten_multi_shell.html '
else:
    emcc_cmd = 'emcc ' + shared_flags + emc_flags + '--pre-js build/assets.js -s FORCE_FILESYSTEM=1 -s MAX_WEBGL_VERSION=2 --emrun -s ERROR_ON_UNDEFINED_SYMBOLS=0 -s DISABLE_DEPRECATED_FIND_EVENT_TARGET_BEHAVIOR=1 -s ALLOW_MEMORY_GROWTH=1 -s WASM_MEM_MAX=2048MB -s MALLOC=dlmalloc -s WASM=1 -o build/ecs_demo.html --shell-file emscripten_shell.html '

#emcc_cmd = 'emcc -v ' + shared_flags + emc_flags + '-s MAX_WEBGL_VERSION=2 --emrun -s ERROR_ON_UNDEFINED_SYMBOLS=0 -s DISABLE_DEPRECATED_FIND_EVENT_TARGET_BEHAVIOR=1 -s ALLOW_MEMORY_GROWTH=1 -s WASM_MEM_MAX=2048MB -s MALLOC=dlmalloc -s WASM=1 -o build/ecs_demo.html --shell-file emscripten_shell.html '
#-s ALLOW_MEMORY_GROWTH=1 -s PROXY_TO_PTHREAD=1 -Wl,--no-check-features -s ERROR_ON_UNDEFINED_SYMBOLS=0 -s TOTAL_MEMORY=512MB 
#-s MAX_WEBGL_VERSION=2

emcc_cmd += '../ecs.bc '
emcc_cmd += 'utils.bc '
emcc_cmd += 'bindbc-sdl.bc '
emcc_cmd += 'glad.bc '
emcc_cmd += 'cimgui.bc '
emcc_cmd += 'mmutils.bc '
emcc_cmd += 'demo.bc '

os.system("mkdir build")

emscripten = imp.load_source('', os.path.expanduser("~") + '/.emscripten')
pack_cmd = emscripten.EMSCRIPTEN_ROOT + '/tools/file_packager.py build/assets.data --preload assets --js-output=build/assets.js'
print('Packafing files: ' + pack_cmd)

os.system(pack_cmd)

if multi:
    final_cmd = emcc_cmd.format('','build/ecs_demo.html')
    print(final_cmd)
    os.system(final_cmd)
    final_cmd = emcc_cmd.format('-s USE_PTHREADS=1','build/ecs_demo_mt.js')
    print(final_cmd)
    os.system(final_cmd)
else:
    print(emcc_cmd)
    os.system(emcc_cmd)
    
os.system('rm build/assets.js')

