
#ifdef GLES
    precision highp float;
    precision highp int;
    precision lowp sampler2D;
    precision lowp samplerCube;
	#if __VERSION__ >290
        #define LOC(x) layout(location = x)
        #define ATT in
        #define M_OUT out mediump
        #define L_OUT out lowp
	#else
        #define LOC(x)
        #define ATT attribute
        #define M_OUT varying mediump
        #define L_OUT varying lowp
	#endif
#else
	#if __VERSION__ > 320
        #define LOC(x) layout(location = x)
        #define ATT in
        #define M_OUT out
        #define L_OUT out
	#else
        #define LOC(x)
        #define ATT attribute
        #define M_OUT varying
        #define L_OUT varying
	#endif
#endif
/*
#ifdef GLES
	#if __VERSION__ >290
		uniform vec4 matrix_1;
		uniform vec4 matrix_2;
		uniform vec4 uv_transform;
		
		layout(location = 0) in vec2 positions;
		layout(location = 1) in vec2 tex_coords;
		
		out mediump vec2 uv;
	#else
		uniform vec4 matrix_1;
		uniform vec4 matrix_2;
		uniform vec4 uv_transform;
		
		attribute vec2 positions;
		attribute vec2 tex_coords;
		
		varying mediump vec2 uv;
	#endif
#else
	#if __VERSION__ > 320
		uniform vec4 matrix_1;
		uniform vec4 matrix_2;
		uniform vec4 uv_transform;
		
		layout(location = 0) in vec2 positions;
		layout(location = 1) in vec2 tex_coords;
		
		out vec2 uv;
	#else
		uniform vec4 matrix_1;
		uniform vec4 matrix_2;
		uniform vec4 uv_transform;
		
		attribute vec2 positions;
		attribute vec2 tex_coords;
		
		varying vec2 uv;
	#endif
#endif*/

#define VBO_BATCH 1

M_OUT vec2 uv;
L_OUT vec4 color;

LOC(0) ATT vec2 positions;
LOC(1) ATT vec2 tex_coords;

#ifdef VBO_BATCH
    LOC(2) ATT float depth;
    LOC(3) ATT vec4 vcolor;
#else
    uniform vec4 matrix_1;
    uniform vec4 matrix_2;
    uniform vec4 uv_transform;
    uniform vec4 vcolor;
    
    float depth = matrix_2.z;
#endif

void main() {
	#ifdef VBO_BATCH
        vec3 position = vec3(positions*4.0,1.0);
        uv = tex_coords;
    #else
        vec3 position = mat3(matrix_1.x,matrix_1.y,0,matrix_1.z,matrix_1.w,0,matrix_2.xy,1.0) * vec3(positions,1.0);
        uv = tex_coords * uv_transform.zw + uv_transform.xy;
    #endif
	
	color = vcolor * 2.0;
    
	gl_Position = vec4(position.xy,depth,1.0);
	
}
