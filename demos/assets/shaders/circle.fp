
#ifdef GLES
    precision mediump int;
    precision mediump float;
    precision lowp sampler2D;
    precision lowp samplerCube;
    #define TEX(x,y) texture2D(x,y)
	#if __VERSION__ >290
		#define M_IN in mediump
		#define L_IN in lowp
	#else
        #define M_IN varying mediump
        #define L_IN varying lowp
	#endif
#else
    #define TEX(x,y) texture(x,y)
	#if __VERSION__ > 320
        #define M_IN in
        #define L_IN in
	#else
        #define M_IN varying
        #define L_IN varying
	#endif
#endif

M_IN vec2 pos;
M_IN float edge;
//flat M_IN vec2 fpos;

//M_IN vec2 uv;
//M_IN vec4 color;
/*
#ifdef GLES
	#if __VERSION__ >290
		in mediump vec2 uv;
	#else
		varying mediump vec2 uv;
	#endif
#else
	#if __VERSION__ > 320
		in vec2 uv;
	#else
		varying vec2 uv;
	#endif
#endif*/

//layout(binding = 0)uniform sampler2D tex;

//uniform sampler2D tex;

//layout(location = 0) out vec4 outColor;

void main() 
{
    float len2 = dot(pos,pos);
    
    if(len2 > 1.0)discard;
    
    if(len2 > edge)gl_FragColor = vec4(0.4,0.8,1.0,0.8);//TEX(tex,uv) * color;
    else gl_FragColor = vec4(0,0.6,1.0,0.35);//TEX(tex,uv) * color;
    //gl_FragColor = vec4(pos,0,1);
	//if(gl_FragColor.a < 0.01)discard;
}
