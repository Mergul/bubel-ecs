
#ifdef GLES
    precision mediump int;
    precision mediump float;
    precision lowp sampler2D;
    precision lowp samplerCube;
    #define TEX(x,y) texture2D(x,y)
	#if __VERSION__ >290
		#define M_IN in mediump
		#define L_IN in lowp
	#else
        #define M_IN varying mediump
        #define L_IN varying lowp
	#endif
#else
	#if __VERSION__ > 320
        #define TEX(x,y) texture(x,y)
        #define M_IN in
        #define L_IN in
	#else
        #define TEX(x,y) texture2D(x,y)
        #define M_IN varying
        #define L_IN varying
	#endif
#endif


M_IN vec2 uv;
M_IN vec4 color;
/*
#ifdef GLES
	#if __VERSION__ >290
		in mediump vec2 uv;
	#else
		varying mediump vec2 uv;
	#endif
#else
	#if __VERSION__ > 320
		in vec2 uv;
	#else
		varying vec2 uv;
	#endif
#endif*/

//layout(binding = 0)uniform sampler2D tex;

uniform sampler2D tex;

//layout(location = 0) out vec4 outColor;

void main() 
{
    gl_FragColor  = TEX(tex,uv) * color;
	if(gl_FragColor.a < 0.01)discard;
}
