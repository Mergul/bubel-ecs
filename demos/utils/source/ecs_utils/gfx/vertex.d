module ecs_utils.gfx.vertex;

import bubel.ecs.std;

struct Vertex 
{
    void create()
    {
        data = Mallocator.make!Data;
    }

    void bind()
    {

    }

    void enableStates()
    {
        
    }

    void attachBindings(scope Binding[] bindings)
    {
        data.bindings = Mallocator.makeArray(bindings);
    }

    enum Type
    {
        byte_r_snorm,
        byte_r_unorm,
        byte_rg_snorm,
        byte_rg_unorm,
        byte_rgb_snorm,
        byte_rgb_unorm,
        byte_rgba_snorm,
        byte_rgba_unorm,
        short_r_snorm,
        short_r_unorm,
        short_rg_snorm,
        short_rg_unorm,
        short_rgb_snorm,
        short_rgb_unorm,
        short_rgba_snorm,
        short_rgba_unorm,
        float_r,
        float_rg,
        float_rgb,
        float_rgba
    }

    struct Binding
    {
        Type type;
        uint stride;
    }

    struct Data
    {
        Binding[] bindings;
        uint size;
    }

    Data* data;
}