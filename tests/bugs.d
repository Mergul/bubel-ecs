module tests.bugs;

import tests.basic;

import bubel.ecs.core;
import bubel.ecs.manager;
import bubel.ecs.system;
import bubel.ecs.attributes;

version(GNU)
{
	pragma(inline, true) T[n] staticArray(T, size_t n)(auto ref T[n] a)
	{
		return a;
	}
}
else import std.array : staticArray;

@("Bug0001")
unittest
{
    struct Event1
    {
        // mixin ECS.Event;

        EntityID id;
    }

    struct Event2
    {
        // mixin ECS.Event;
    }

    struct System1
    {
        mixin ECS.System;

        struct EntitiesData
        {
            CInt[] int_;
        }

        EntityTemplate* tmpl;
        EntityID id;

        void onCreate()
        {
            tmpl = gEntityManager.allocateTemplate([becsID!CInt, becsID!CLong].staticArray);
        }

        void onDestroy()
        {
            gEntityManager.freeTemplate(tmpl);
        }

        void handleEvent(Entity* entity, Event1 event)
        {
            gEntityManager.removeEntity(event.id);
            gEntityManager.sendEvent(entity.id,Event2());
        }

        void handleEvent(Entity* entity, Event2 event)
        {
            id = gEntityManager.addEntity(tmpl).id;
        }
    }

    struct System2
    {
        mixin ECS.System;

        struct EntitiesData
        {
            Entity[] entity;
        }

        ///check if every entity was removed correctly
        void onUpdate(EntitiesData data)
        {
            assert(0);
        }
    }

    struct System3
    {
        mixin ECS.System;

        struct EntitiesData
        {
            uint length;
            Entity[] entity;
        }

        ///remove every entity
        void onUpdate(EntitiesData data)
        {
            foreach(i;0..data.length)gEntityManager.removeEntity(data.entity[i].id);
        }
    }

    gEntityManager.initialize(0);

    gEntityManager.beginRegister();

    gEntityManager.registerComponent!CInt;
    gEntityManager.registerComponent!CFloat;
    gEntityManager.registerComponent!CDouble;
    gEntityManager.registerComponent!CLong;
    gEntityManager.registerComponent!CShort;
    gEntityManager.registerComponent!CFlag;

    gEntityManager.registerEvent!Event1;
    gEntityManager.registerEvent!Event2;

    gEntityManager.registerSystem!System1(0);
    gEntityManager.registerSystem!System2(-200);
    gEntityManager.registerSystem!System3(-200);

    gEntityManager.endRegister();

    EntityTemplate* tmpl = gEntityManager.allocateTemplate([becsID!CInt, becsID!CLong].staticArray);

    CLong clong = CLong(10);
    CInt cint = CInt(6);
    CInt cint2 = CInt(4);
    EntityID id = gEntityManager.addEntity(tmpl,[ComponentRef(&clong, becsID(clong)), ComponentRef(&cint, becsID(cint))].staticArray).id;
    EntityID id2 = gEntityManager.addEntity(tmpl,[ComponentRef(&cint2, becsID(cint2))].staticArray).id;
    gEntityManager.freeTemplate(tmpl);
    gEntityManager.commit();

    gEntityManager.sendEvent(id2, Event1(id));

    gEntityManager.getSystem(becsID!System2).disable();

    gEntityManager.begin();
    gEntityManager.update();
    gEntityManager.end();

    gEntityManager.getSystem(becsID!System2).enable();

    gEntityManager.begin();
    gEntityManager.update();
    gEntityManager.end();

    gEntityManager.destroy();
}

@("2-empty-entity-crash")
unittest
{
    
    gEntityManager.initialize(0);

    gEntityManager.beginRegister();

    gEntityManager.registerComponent!CInt;
    gEntityManager.registerComponent!CFloat;

    gEntityManager.endRegister();


    EntityTemplate* tmpl = gEntityManager.allocateTemplate([becsID!CInt, becsID!CFloat].staticArray);
    scope(exit) gEntityManager.freeTemplate(tmpl);

    EntityID id = gEntityManager.addEntity(tmpl).id;
    // EntityID id2 = gEntityManager.addEntity().id;

    gEntityManager.commit();

    gEntityManager.removeComponents(id, [becsID!CInt, becsID!CFloat].staticArray);

    gEntityManager.commit();

    gEntityManager.destroy();
}