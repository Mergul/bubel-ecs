module tests.perf;

import tests.runner;

import bubel.ecs.core;
import bubel.ecs.manager;
import bubel.ecs.entity;

version(GNU)
{
	pragma(inline, true) T[n] staticArray(T, size_t n)(auto ref T[n] a)
	{
		return a;
	}
}
else import std.array : staticArray;

import core.stdc.stdio;

struct CLong
{
    mixin ECS.Component;

    alias value this;

    long value = 10;
}

struct CShort
{
    mixin ECS.Component;

    alias value this;

    short value = 12;
}

struct CInt
{
    mixin ECS.Component;

    alias value this;

    int value = 10;
}

struct CUInt
{
    mixin ECS.Component;

    alias value this;

    uint value = 12;
}

struct CBig
{
    mixin ECS.Component;
    uint[32] data;
}

EntityTemplate* tmpl;

void beforeEveryTest()
{
    gEntityManager.initialize(0);

    gEntityManager.beginRegister();

    gEntityManager.registerComponent!CLong;
    gEntityManager.registerComponent!CShort;
    gEntityManager.registerComponent!CInt;
    gEntityManager.registerComponent!CUInt;
    gEntityManager.registerComponent!CBig;

    gEntityManager.endRegister();
    tmpl = null;
}

void afterEveryTest()
{
    if(tmpl)gEntityManager.freeTemplate(tmpl);
    tmpl = null;
    gEntityManager.destroy();
}

void smallTmpl()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CShort].staticArray);
}

void bigTmpl()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CBig].staticArray);
}

void multiSmallTmpl()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CShort, becsID!CLong, becsID!CInt, becsID!CUInt].staticArray);
}

void multiBigTmpl()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CLong, becsID!CInt, becsID!CUInt, becsID!CBig].staticArray);
}

@("AddEntities100k1comp2b")  @(before, &smallTmpl)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k1comp128b")  @(before, &bigTmpl)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k4comp18b")  @(before, &multiSmallTmpl)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k4comp144b")  @(before, &multiBigTmpl)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

void allocDealloc100k()
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
    gEntityManager.commit();
    foreach(i; 0..100_000)gEntityManager.removeEntity(EntityID(i + 1,0));
    gEntityManager.commit();
}

void smallTmplPreAlloc()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CShort].staticArray);
    allocDealloc100k();
}

void bigTmplPreAlloc()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CBig].staticArray);
    allocDealloc100k();
}

void multiSmallTmplPreAlloc()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CShort, becsID!CLong, becsID!CInt, becsID!CUInt].staticArray);
    allocDealloc100k();
}

void multiBigTmplPreAlloc()
{
    tmpl = gEntityManager.allocateTemplate([becsID!CLong, becsID!CInt, becsID!CUInt, becsID!CBig].staticArray);
    allocDealloc100k();
}

@("AddEntities100k1comp2bPreAlloc")  @(before, &smallTmplPreAlloc)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k1comp128bPreAlloc")  @(before, &bigTmplPreAlloc)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k4comp18bPreAlloc")  @(before, &multiSmallTmplPreAlloc)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}

@("AddEntities100k4comp144bPreAlloc")  @(before, &multiBigTmplPreAlloc)
unittest
{
    foreach(i; 0..100_000)gEntityManager.addEntity(tmpl);
}