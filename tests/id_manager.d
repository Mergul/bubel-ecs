module tests.id_manager;

import bubel.ecs.id_manager;
import bubel.ecs.entity;

unittest
{
    IDManager manager;
    manager.initialize();
    EntityID id1 = manager.getNewID();
    EntityID id2 = manager.getNewID();
    EntityID id3 = manager.getNewID();

    assert(id1 == EntityID(1, 0));
    assert(id2 == EntityID(2, 0));
    assert(id3 == EntityID(3, 0));

    manager.optimize();
    manager.releaseID(id2);
    manager.releaseID(id1);

    id2 = manager.getNewID();
    id1 = manager.getNewID();

    Entity e;
    e.id = id3;
    manager.update(e);

    assert(id1 == EntityID(2, 1));
    assert(id2 == EntityID(1, 1));
    assert(id3 == EntityID(3, 0));
    assert(manager.isExist(id3));
    assert(!manager.isExist(EntityID(1, 0)));
    assert(!manager.isExist(EntityID(0, 0)));
    manager.deinitialize();
}
